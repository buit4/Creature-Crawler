import java.util.*;

public class CreatureCrawler {
	//player stats
	static int playerHealth=100;
	static int attack=1;
	static int level=1;
	static int potion=3;
	
	public static void main(String[] args) {
		//initialize main method variables
	Scanner input = new Scanner(System.in);
	Random rand = new Random();
	String walk;
	int x;
	
	//enter dungeon
	System.out.println("[Enter]Go into Dungeon ");
	walk = input.nextLine();
	//moving through dungeon
		while (level<10){
			System.out.println("");
			System.out.println("Enter [W]to go straight, [A]to go to the left, [D]to go to the right ");
			walk = input.next();
	
			//which creature to encounter (rat 40%, skeleton 30%, 3rd 20%, 10% nothing)
			x=rand.nextInt(11);
			//creature(input,creatureName,creatureAtt,creatureHealth,levelIncrease)
			if(x<2){
				creature(input,"Rat",1,3,1);
			}else if(x>=3 && x<5){
				creature(input,"Skeleton",2,4,2);
			}else if(x>=5 && x<6){
				creature(input,"Spider",3,8,3);
			}else if(x>=6 && x<7){
				creature(input,"Troll",8,15,4);
			}
				}
	//boss
		boss(input,rand);
		System.out.print("Congratualations! You Win!");
	}
	
	//creature
	public static void creature(Scanner in,String creatureName,int creatureAtt,int creatureHealth,int levelInc){
		String action;
		System.out.println("A "+creatureName+ " is attacking you! What will you do? ");
		int initialHealth=creatureHealth;
		
		do{
			displayStats(creatureHealth,initialHealth);
			System.out.printf("[Z]Attack (Your Attack: %d)%n  [X]Run (Enemy Attack: %d)%n "
					+ " [C]Potion (Remaining: %d)%n",attack,creatureAtt,potion);
			action=in.next();
			
			if(action.equalsIgnoreCase("Z")){
			creatureHealth-=attack;
			playerHealth-=creatureAtt;
			 }else if(action.equalsIgnoreCase("X")){
				 System.out.println("You're a coward!");
				break;
			 }else if(action.equalsIgnoreCase("C")){
				 if(potion<=0){
					 System.out.println("No more potions.");
				 }else{
				 System.out.println("Used Potion! Health increased by 25!");
				 playerHealth+=25;
				 potion--;
				 	if(playerHealth>100){
				 		playerHealth=100;
				 	}
				 }
			 }
		} while (creatureHealth>0&&playerHealth>0);
	
			if(playerHealth<=0){
				System.out.println("You died! Game Over!");
				System.exit(0);
			}
		
		if(creatureHealth<=0){
			System.out.println("You killed the "+creatureName+"!");
			level+=levelInc;
			attack+=(levelInc*3);
			System.out.println("You leveled up to level "+ level+"!");
			}
	}
	
	public static void boss(Scanner in,Random r){
		String action;
		int initialHealth=100;
		int bossHealth=100;
		int dodge;
	
		System.out.println("");
		System.out.println("A GIANT OGRE TELEPORTED BEHIND YOU!!! ");
		System.out.println("WHAT ARE YOU GOING TO DO!?!?");
		do{
			displayStats(bossHealth,initialHealth);
			System.out.printf("[Z]Attack (Your Attack: %d)%n  [X]Run (Enemy Attack: %d)%n "
					+ " [C]Potion (Remaining: %d)%n",attack,25,potion);
			action=in.next();
			
			if(action.equalsIgnoreCase("Z")){
			dodge=r.nextInt(10);
				if(dodge>3){
					bossHealth-=attack;
					playerHealth-=25;
				}else{
					System.out.println("You missed!");
					playerHealth-=25;
				}
			 }else if(action.equalsIgnoreCase("X")){
				 System.out.println("You tried to run and got rekted.");
				 System.out.print("Game Over.");
				 System.exit(bossHealth);
			 }else if(action.equalsIgnoreCase("C")){
				 if(potion<=0){
					 System.out.println("No more potions.");
				 }else{
				 System.out.println("Used Potion! Health increased by 25!");
				 playerHealth+=25;
				 potion--;
				 	if(playerHealth>100){
				 		playerHealth=100;
				 		}
				 }
			 }
			if(playerHealth<=0){
				System.out.println("You died! Game Over!");
				System.exit(0);
			}
		} while (bossHealth>0);
	}
	//Show the health of player and enemy
	public static void displayStats(int enemyHealth,int initialHealth){
		System.out.print("Enemy Health: "+enemyHealth+"/"+initialHealth);
		System.out.println(" ---- Your Health: "+playerHealth+"/100");
	}
}